export interface EventDetail {
    id: number;
    eventTime: string;
    eventLocation: string;
    fightCards: [any];
}
