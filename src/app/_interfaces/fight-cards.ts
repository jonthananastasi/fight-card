export interface FightCards {
    id: number;
    eventLocation: string;
    eventTime: string;
    fightTime: string;
    fightImage: string;
    fightType: string;
    firstFighterFirstName: string;
    firstFighterSurname: string;
    secondFighterFirstName: string;
    secondFighterSurname: string;
    mainFightCard: boolean;
}
