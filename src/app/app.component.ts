import {Component, OnInit} from '@angular/core';
import {ApiService} from "./_services/api.service";
import {FightCards} from "./_interfaces/fight-cards";
import {EventDetail} from "./_interfaces/event-detail";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    fightCards!: FightCards;
    eventDetail!: EventDetail;

    constructor(private apiService: ApiService) {
    }

    ngOnInit(): void {
        this.doGetEvent();
    }

    /**
     * Gets Event Information
     */
    doGetEvent() {
        this.apiService.getEventDetails().subscribe({
            next: (res: any) => {
                this.eventDetail = res
            },
            error: (err) => {
                console.log(err, 'Error')
            }
        });
    }
}
