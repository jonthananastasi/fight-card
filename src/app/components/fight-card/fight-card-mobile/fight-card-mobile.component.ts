import {Component, Input, OnInit} from '@angular/core';
import {EventDetail} from "../../../_interfaces/event-detail";

@Component({
    selector: 'app-fight-card-mobile',
    templateUrl: './fight-card-mobile.component.html',
    styleUrls: ['./fight-card-mobile.component.scss'],
})

export class FightCardMobileComponent implements OnInit {
    @Input() eventDetail!: EventDetail;
    singleCardSelected!: number;
    fightCardLength!: number;

    constructor() {
    }

    ngOnInit(): void {
        this.singleCardSelected = 0;
    }

    /**
     * Selects a single fight card on hover / click
     */
    doSelectSingleCard(i: number) {
        this.singleCardSelected = i;
    }

    /**
     * Swipes left to the next fight card.
     */
    swipeLeft(i: number) {
        this.fightCardLength = this.eventDetail?.fightCards.length;
        if (this.singleCardSelected >= this.fightCardLength - 1) {
            return;
        } else {
            this.doSelectSingleCard(i + 1);
        }
    }

    /**
     * Swipes rights to the next fight card.
     */
    swipeRight(i: number) {
        if (this.singleCardSelected == 0) {
            return;
        } else
            this.doSelectSingleCard(i - 1);
    }
}
