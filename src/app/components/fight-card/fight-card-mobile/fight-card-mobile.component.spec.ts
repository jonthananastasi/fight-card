import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FightCardMobileComponent} from './fight-card-mobile.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('FightCardMobileComponent', () => {
    let component: FightCardMobileComponent;
    let fixture: ComponentFixture<FightCardMobileComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [FightCardMobileComponent],
            imports: [HttpClientTestingModule]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(FightCardMobileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    it(`should set singleCardSelected to 0 on ngOnInit`, () => {
        expect(component.singleCardSelected).toEqual(0);
    });
});
