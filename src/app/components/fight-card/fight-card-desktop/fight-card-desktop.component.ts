import {Component, Input, OnInit} from '@angular/core';
import {EventDetail} from "../../../_interfaces/event-detail";

@Component({
    selector: 'app-fight-card-desktop',
    templateUrl: './fight-card-desktop.component.html',
    styleUrls: ['./fight-card-desktop.component.scss']
})
export class FightCardDesktopComponent implements OnInit {
    @Input() eventDetail!: EventDetail;
    singleCardSelected!: number;

    constructor() {
    }

    ngOnInit(): void {
        this.singleCardSelected = 0;
    }

    /**
     * Selects a single fight card on hover / click
     */
    doSelectSingleCard(i: number) {
        this.singleCardSelected = i;
    }
}
