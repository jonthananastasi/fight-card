import {NgModule} from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {FightCardDesktopComponent} from "./components/fight-card/fight-card-desktop/fight-card-desktop.component";
import {FightCardMobileComponent} from "./components/fight-card/fight-card-mobile/fight-card-mobile.component";
import 'hammerjs';

@NgModule({
    declarations: [
        AppComponent,
        FightCardDesktopComponent,
        FightCardMobileComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule, HttpClientModule, HammerModule
    ],
    providers: [{provide: HAMMER_GESTURE_CONFIG, useClass: HammerGestureConfig, deps: []}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
