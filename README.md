# Fight Card

Build the ‘Fight Card’ component as a fully responsive single page application. Build with Angular 13.0   

## To Run The App

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Data

The JSON data file can be found `.assets/data/eventDetailData.json`.

## Components

In the `fight-card` folder you will find two components `fight-card-desktop` & `fight-card-mobile`. Either component will be loaded and displayed in the `app.component.html` based on the 
width of the screen

## Services

The `app/_services/api.service.ts` is where the http call is made to retrieve the eventDetailData.json.

## Mobile

Simulate a mobile viewport in Chrome to access the swipe capabilities for mobile scrolling. (uses hammerjs)

## Unit Test

Added some basic unit tests which are: 
- FightCardMobileComponent `should set singleCardSelected to 0 on ngOnInit`.
- FightCardDesktopComponent `should select and set doSelectSingleCard to 1`.


